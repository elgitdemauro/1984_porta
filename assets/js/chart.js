        $(function () {
            $('#container').highcharts({

                chart: {
                    type: 'bubble',
                    plotBorderWidth: 0,
                    zoomType: 'xy'
                },
                legend: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                xAxis: {
                    gridLineColor: '#fff',
                    lineColor: '#fff',
                    minorGridLineColor: '#fff',
                    tickColor: '#fff',
                    plotLines: [{
                        color: '#fff'
                    }],
                    gridLineWidth: 0,
                    startOnTick: false,
                    endOnTick: false,
                    title: {
                        text: ''
                    },
                    labels: {
                        format: '',//'{value} gr'
                        style: {
                            color: '#fff'
                        }
                    },
                },

                yAxis: {
                    gridLineColor: '#F3F3F3',
                    lineColor: '#F3F3F3',
                    minorGridLineColor: '#F3F3F3',
                    tickColor: '#F3F3F3',
                    plotLines: [{
                        color: '#F3F3F3'
                    }],

                    gridLineWidth: 0,
                    startOnTick: false,
                    endOnTick: false,
                    title: {
                        text: ''
                    },
                    labels: {
                        format: '' ,//'{value} gr'
                        style: {
                            color: '#fff'
                        }
                    },
                    maxPadding: 0.2
                },

                tooltip: {
                    useHTML: true,
                    headerFormat: '<div class="high_hover">',
                    pointFormat: '<h3>{point.name}</h3>' + '<p><strong>{point.child} </strong></p>',
                    footerFormat: '</div>',
                    followPointer: true,
                    borderRadius: '6'
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'

                        }
                    }
                },
                credits: {
                    enabled: false
                },
                series: [{
                    data: [
                        {
                            x: 28,
                            y: 70,
                            z: 90,
                            name: 'UI',
                            color: '#E7C1D9',
                            child: ['Bootstrap', ' Material Design']
                        },
                        {
                            x: 22,
                            y: 90,
                            z: 100,
                            name: 'Responsive',
                            child: ['Cross browser'],
                            color: '#2ecc71'
                        },
                        {
                            x: 36,
                            y: 85,
                            z: 100,
                            name: 'CSS',
                            child: ['PostCSS',' Compilers'],
                            color: '#3498db'
                        },
                        {
                            x: 45,
                            y: 70,
                            z: 80,
                            name: 'JS',
                            color: '#f1c40f' ,
                            child: 'Javascript & Frameworks'
                        },
                        {
                            x: 55,
                            y: 80,
                            z: 70,
                            name: 'Nodejs',
                            color: '#e67e22',
                            child: ['NPM', ' Grunt', ' Gulp']
                        },
                        {
                            x: 40,
                            y: 55,
                            z: 80,
                            name: 'AngularJs',
                            country: 'AngularJs',
                            color: '#B2F372',
                            child: ['RESTful APIs']
                        },
                        {
                            x: 60,
                            y: 65,
                            z: 60,
                            name: 'Git',
                            color: '#FF9CB8'
                        },
                        {
                            x: 70,
                            y: 50,
                            z: 45,
                            name: 'Wordpress',
                            country: 'Wordpress',
                            color: '#dddddd'
                        },
                        {
                            x: 70,
                            y: 74,
                            z: 22,
                            name: 'Ruby',
                            country: 'Ruby',
                            color: '#ff0000' ,
                            child: 'Middleman'
                        },
                        {
                            x: 77,
                            y: 60,
                            z: 20,
                            name: 'ReactJs',
                            country: 'ReactJs',
                            color: '#A0D6E4' ,
                            child: 'Rails (Noob)'
                        },
                        {
                            x: 55,
                            y: 50,
                            z: 40,
                            name: 'Php'
                        }
                    ]
                }]

            });
        });
