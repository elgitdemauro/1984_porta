$( document ).ready(function() {
    // ANIMATION
    $('#ready').css('display', 'block').addClass('fadeIn');


    // CLIENTS
    $('#last_project .toggle').click(function() {
        //$(this).children('.button_container').toggleClass('active');
        $(this).children('.overlay').toggleClass('open');
        if ($(".overlay").is(".open")){
            $('.button_container').addClass('active');
        }else{
            $('.button_container').removeClass('active');
        }
    });


    $('[data-toggle="tooltip"]').tooltip({
        delay: {
            "show": 250,
            "hide": 100
        }
    });
});


// TYPE
$(function(){
    $("#typed").typed({
        //strings: ["Typed.js is a <strong>jQuery</strong> plugin.", "It <em>types</em> out sentences.", "And then deletes them.", "Try it out!"],
        //stringsElement: $('#typed-strings'),
        strings: ["<small><<i>h2</i>></small>Hello<small>&lt;/<i>h2</i>></small>\n<small><<i>h1</i>></small>I am Mauro Avendaño<small>&lt;/<i>h1</i>></small>\n<small><<i>h2</i>></small>Front-end Developer<small>&lt;/<i>h2</i>></small>"],
        typeSpeed: 30,
        backDelay: 5500,
        loop: false,
        contentType: 'html', // or text
        cursorChar: "_",
        loopCount: false,
        showCursor: true,
            //callback: function(){ foo(); },
            resetCallback: function() { newTyped(); }
    });

    $(".reset").click(function(){
        $("#typed").typed('reset');
    });

});

function newTyped(){ /* A new typed object */ }
//function foo(){ console.log("Callback"); }

$(function (){
    var text = "";
    var i;
    for (i = 1; i < 100; i++) {
        text +=  + i + "<br>";
    }
    document.getElementById("line").innerHTML = text;
});
