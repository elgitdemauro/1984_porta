/*var connect = require('connect');
var serveStatic = require('serve-static');

connect().use(serveStatic(__dirname)).listen(80);
console.log('Ready');
*/


var express 		= require('express');
var app     		= express();
var mongojs 		= require('mongojs');

app.use(express.static(__dirname + "/"));

app.listen(80);
console.log('Server running port 80');
