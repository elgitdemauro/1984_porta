(function () {
    var clock, currentDate, diff, futureDate;
    currentDate = new Date();
    futureDate = new Date('April 18, 2016  24:00:00');
    diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;
    clock = $('.clock').FlipClock(diff, {
        clockFace: 'DailyCounter',
        countdown: true,
        showSeconds: true,
        language:'es-es'
    });
}.call(this));