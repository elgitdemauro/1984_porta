$( document ).ready(function() {

  /*
    MODAL
  */
  $('#myModal').on('shown.bs.modal', function () {
    $('#myInput').focus()
  });



  /*
    IMG TO YOUTUBE
  */
  function getYoutubeID(url) {
    var id = url.match("[\\?&]v=([^&#]*)");
    id = id[1];
    return id;
  };

  $('div.you').click(function(event) {
    var src = $(this).css('background'); // "static/images/banner/blue.jpg"
    console.log(src);
    var tarr = src.split('/');      // ["static","images","banner","blue.jpg"]
    console.log(tarr);
    var file = tarr[tarr.length-3]; // "blue.jpg"
    console.log('file: '+file);
    var data = file.split('.')[0];  // "blue"
    console.log(data);
    var iframeSrc = "https://www.youtube.com/embed/"+data+"?autoplay=1";
    console.log(iframeSrc);
    $('.iframe').attr('src', iframeSrc);

    $("#myModal").on('hidden.bs.modal', function (e) {
      $("#myModal iframe").attr("src", $("#myModal iframe").attr("src", ""));
    });
  });


});



/*
  IMG SVG STYLE
*/
$(function(){
  jQuery('img.svg').each(function(){
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    jQuery.get(imgURL, function(data) {
      // Get the SVG tag, ignore the rest
      var $svg = jQuery(data).find('svg');    
      // Add replaced image's ID to the new SVG
      if(typeof imgID !== 'undefined') {
        $svg = $svg.attr('id', imgID);
      }
      // Add replaced image's classes to the new SVG
      if(typeof imgClass !== 'undefined') {
        $svg = $svg.attr('class', imgClass+' replaced-svg');
      }
      // Remove any invalid XML tags as per http://validator.w3.org
      $svg = $svg.removeAttr('xmlns:a');
      // Check if the viewport is set, else we gonna set it if we can.
      if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
        $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
      }
      // Replace image with new SVG
      $img.replaceWith($svg);

    }, 'xml');

  });
});



/*
  MAX NUMBER INPUT RUT
*/
function maxLengthCheck(object) {
  if (object.value.length > object.maxLength)
    object.value = object.value.slice(0, object.maxLength)
};
function isNumeric (evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode (key);
  var regex = /[0-9]|\./;
  if ( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
};








