<?php
  require_once("assets/conexion/conexion.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="icon" type="image/png" href="assets/img/favicon.png"> -->
    <title>Copa Coca-Cola</title>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="assets/css/main.css">
  </head>
  <body onload="limpiar()">
    <section class="cover">
      <div class="container">
        <div class="col-sm-3 center">
          <a href="index.html">
            <img src="assets/img/img_logo_copacocacola.png" class="img-responsive" border="0" alt="">
          </a>
        </div>
        <div class="col-sm-9">
          <div class="social">
            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="#" target="_blank"><i class="fa fa-instagram"></i></a>
          </div>
          <div class="slogan">
            <img src="assets/img/img_cover_slogan.png" class="img-responsive pull-right" border="0" alt="">
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
              </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav">
                <li><a href="index.html">Home</a></li>
                <li><a href="inscripciones.html" class="active">Inscripci&oacute;n</a></li>
                <li><a href="bases.html">Bases</a></li>
                <li><a href="noticias.html">Noticias</a></li>
                <li><a href="regiones.html">Regiones</a></li>
                <li><a href="galeria.html">Galer&iacute;a</a></li>
                <li><a href="fixture.html">Fixture</a></li>
                <li><a href="preguntas_frecuentes.html">Preguntas Frecuentes</a></li>
                <li><a href="http://www.acciontotal.cl/copacocacola/" target="_blank">Copa 2015</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </div>  
    </section>

    <section class="interna">
      <div class="container">
        <h1><i class="fa fa-pencil-square-o"></i> Inscripciones</h1>

        <div class="content_interna">
          <h3>Pre-Inscr&iacute;bete en nuestra copa</h3>
          <p>Completa todos los datos que aparecen a continuaci&oacute;n y se parte del campeonato escolar m&aacute;s grande de Chile</p>

          <form action="registro.php" name="form" method="POST" role="form" class="form" accept-charset="utf-8">
            <div class="col-sm-6 form-group">
              <input type="text" name="nombres" class="form-control" placeholder="Nombres" >
            </div>
            <div class="col-sm-6 form-group">
              <input type="text" name="apellidos" class="form-control" placeholder="Apellidos" >
            </div>
            <div class="col-sm-6 form-group">
              <input type="number" name="rut" class="form-control" id="rut" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" type = "number" maxlength = "9" min = "1" placeholder="RUT (ej: 181234567)" >
            </div>
            <div class="col-sm-6 form-group">
              <input type="text" name="email" class="form-control" placeholder="E-Mail" >
            </div>
            <div class="col-sm-6 form-group">
              <input type="text" name="celular" class="form-control" placeholder="Celular" >
            </div>
            <div class="col-sm-6 form-group">
              <input type="number" name="telefono_fijo" class="form-control" placeholder="Fono Fijo (Opcional)">
            </div>
            <div class="col-sm-6 form-group">
              <?php
                $sql="SELECT * FROM region_cl ORDER BY id_re ASC";
                $res=mysql_query($sql,$con);
              ?>
              <select name="region" class="form-control" onchange="from(document.form.region.value,'provincia','provincias.php')">
                <option value="0">-- Seleccione la regi&oacute;n --</option>
                <?php
                  while ($reg=mysql_fetch_array($res))
                  {
                ?>
                <option value="<?php echo $reg['id_re'];?>">
                  <?php echo $reg["str_romano"];?> - 
                  <?php echo $reg["str_descripcion"];?>
                </option>
                <?php
                  }
                ?>
              </select>
            </div>
            <div class="col-sm-6 form-group" id="provincia">
              <select name="provincia" class="form-control" required="required">
                <option value="0">-- Seleccione una provincia --</option>
              </select>
            </div>
            <div class="col-sm-12 form-group" id="comuna">
              <select name="comuna" class="form-control" required="required">
                <option value="0">-- Seleccione una comuna --</option>
              </select>
            </div>
            <div class="col-sm-12 form-group">
              <input type="text" name="institucion" class="form-control" placeholder="Instituci&oacute;n">
            </div>
            
            <div class="clear"></div><br>
            <h4>Tipo de juego</h4>
            <p>Puedes seleccionar m&aacute;s de uno</p>
            <div class="checkbox">

              <label>
                <input type="checkbox" name="tipo_juego[]" value="varones">
                Hombres (F&uacute;tbol II)
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="tipo_juego[]" value="damas">
                Mujeres (Futbolito)
              </label>
            </div>

            <div class="clear"></div><br>
            <h4>¿Tu colegio tiene cancha de F&uacute;tbol?</h4>
            <div class="radio">
              <label>
                <input type="radio" name="colegio_cancha" value="si">
                Si
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="colegio_cancha" value="no">
                No
              </label>
            </div>
            
            <br>
            <a href="tabla.php">Excel</a>
            <br>
            
            <div class="col-sm-4 col-sm-offset-4">
              <button type="submit" class="btn btn-danger btn-block btn_send">Enviar</button>
            </div>
          </form>
        </div>

      </div>
    </section>



    <footer>
      <div class="top">
        <div class="container">
          <div class="col-sm-5">
            <p>Produce</p>
            <img src="assets/img/logo_acciontotal.png" class="img-responsive" border="0" alt="">
            <img src="assets/img/logo_copacocacola_sm.png" class="img-responsive" border="0" alt="">
          </div>
          <div class="col-sm-4">
            <p>Colabora</p>
            <img src="http://fakeimg.pl/110x70/999/FFF/?text=logo" class="img-responsive" border="0" alt="">
          </div>
          <div class="col-sm-3">
            <p>Redes</p>
            <div class="social">
              <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
              <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
              <a href="#" target="_blank"><i class="fa fa-instagram"></i></a>
            </div>
          </div>
        </div>
      </div>
      <div class="bottom">
        <div class="container center">
          <img src="http://fakeimg.pl/600x100/999/FFF/?text=banner+Mindep" class="img-responsive" border="0" alt="">  
        </div>          
      </div>   
    </footer>

    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="assets/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/funciones.js"></script>
    <script src="assets/js/scripts.js"></script>

  </body>
</html>
