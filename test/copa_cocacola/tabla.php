<?php
 /* Ejemplo 1 generando excel desde mysql con PHP
    @Autor: Carlos Hernan Aguilar Hurtado
 */


    $conexion = mysql_connect ("localhost", "root", "root");
    mysql_select_db ("cocacola_inscripcion", $conexion);  
    mysql_set_charset("UTF8", $conexion);
    $sql = "SELECT * FROM form ";
    $resultado = mysql_query ($sql, $conexion) or die (mysql_error ());
    $registros = mysql_num_rows ($resultado);
    

    if ($registros > 0) {
     require_once 'Classes/PHPExcel.php';
     $objPHPExcel = new PHPExcel();

   //Informacion del excel
     $objPHPExcel->
     getProperties()
     ->setCreator("mauro")
     ->setLastModifiedBy("")
     ->setTitle("Exportar excel desde mysql")
     ->setSubject("")
     ->setDescription("")
     ->setKeywords("")
     ->setCategory("");    


     $h = 1;
     $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A'.$h, 'ID')
      ->setCellValue('B'.$h, 'NOMBRES')
      ->setCellValue('C'.$h, 'APELLIDOS')
      ->setCellValue('D'.$h, 'RUT')
      ->setCellValue('E'.$h, 'EMAIL')
      ->setCellValue('F'.$h, 'CELULAR')
      ->setCellValue('G'.$h, 'TELEFONO_FIJO')
      ->setCellValue('H'.$h, 'REGION')
      ->setCellValue('I'.$h, 'PROVINCIA')
      ->setCellValue('J'.$h, 'COMUNA')
      ->setCellValue('K'.$h, 'INSTITUCION')
      ->setCellValue('L'.$h, 'TIPO_JUEGO')
      ->setCellValue('M'.$h, 'COLEGIO_CANCHA');

    $objPHPExcel ->getActiveSheet()
                 ->getStyle('A1:M1')
                 ->getFill()
                 ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                 ->getStartColor()
                 ->setARGB('FF17eab5');
    
    $objPHPExcel ->getActiveSheet()
                 ->getRowDimension('1')
                 ->setRowHeight(40);

    $objPHPExcel ->getActiveSheet()
                 ->getStyle('A1:M1')
                 ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

     $c = 2;    
     while ($registro = mysql_fetch_object ($resultado)) {
      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$c, $registro->id)
        ->setCellValue('B'.$c, $registro->nombres)
        ->setCellValue('C'.$c, $registro->apellidos)
        ->setCellValue('D'.$c, $registro->rut)
        ->setCellValue('E'.$c, $registro->email)
        ->setCellValue('F'.$c, $registro->celular)
        ->setCellValue('G'.$c, $registro->telefono_fijo)
        ->setCellValue('H'.$c, $registro->region)
        ->setCellValue('I'.$c, $registro->provincia)
        ->setCellValue('J'.$c, $registro->comuna)
        ->setCellValue('K'.$c, $registro->institucion)
        ->setCellValue('L'.$c, $registro->tipo_juego)
        ->setCellValue('M'.$c, $registro->colegio_cancha);  
      $c++;

    }
  }
  header('Content-Type: application/vnd.ms-excel; charset=utf-8');
  header('Content-Disposition: attachment;filename="registro_inscripcion.xlsx"');
  header('Cache-Control: max-age=0');

  $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
  $objWriter->save('php://output');
  exit;
  mysql_close ();
  ?>