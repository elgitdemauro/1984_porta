$(document).ready(function() {
  
  /* ANIMATION CATEGORY */
  $('.title').addClass('fadeIn');
  setTimeout(function(){
    $('#Comida').addClass('pulse');
  },500);
  setTimeout(function(){
    $('#Tecnologia').addClass('pulse');
  },1000);
  setTimeout(function(){
    $('#Vestuario').addClass('pulse');
  },1500);
  setTimeout(function(){
    $('#Deporte').addClass('pulse');
  },2000);

  
   
  var showId;

  $('.article').click(function(e){
    e.preventDefault();
    
    showId = $(this).attr('id');
    
    console.log(showId);
    $('.step1').addClass('fadeOut');
    setTimeout(function(){
      $('.step1').css('display', 'none');
      setTimeout(function(){
        $('.step2').addClass('fadeIn');
      },100)
    }, 100);
    return false;
  });

 
   
  /* FORM VALIDATION */
  $('button[type="submit"]').click(function(e) {
    e.preventDefault();

    var checkbox  = $('input[name="optionsRadios"]');
    var email     = $('input[type="email"]').val();
    
    var validateEmail = function(elementValue) {
      var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
      return emailPattern.test(elementValue);
    }
    
    var sexo  = $('input[name="optionsRadios"]:checked').val();
    var value = email;
    var valid = validateEmail(value);
 
    if( (!checkbox.is(':checked') ) || (!valid) ) {
      
      if(!checkbox.is(':checked') ){
        $('.box_error').css('display', 'block');
        $('label[for="optionsRadios"]').css('display', 'block');
      }else{
        $('.box_error').css('display', 'none');
        $('label[for="optionsRadios"]').css('display', 'none');
      };

      if( email==='' ){
        $('.box_error').css('display', 'block');
        $('label[for="email"]').css('display', 'block');
      }else{
        
        if (!valid) {
          $('.box_error.alert').css('display', 'block');
          $('.box_error.alert label[for="email"]').css('display', 'block');
        } else {
          $('label[for="email"]').css('display', 'none');
        }
      };
      
    } else{
      
      $('.box_error.alert').css('display', 'none');
      alert(showId+'\n'+sexo+'\n'+value);
      
    };
    
  });


}); /* document ready */


