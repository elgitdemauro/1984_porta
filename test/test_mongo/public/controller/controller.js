angular.module('myApp', [])

  .controller('AppCtrl', function($scope, $http){
    console.log('hello World');

    var refresh = function(){
      $http.get('/contactlist')
      .success(function(response){
        console.log('got the data I requested');
        $scope.contactlist = response;
        $scope.contact = '';
      });
    }

    refresh();

    $scope.addContact = function(){
      console.log($scope.contact);
      $http.post('/contactlist', $scope.contact)
        .success(function(response){
          console.log(response);
          refresh();
        })
    };

    $scope.remove = function(id){
      console.log(id);
      $http.delete('/contactlist/' + id)
        .success(function(response){
          console.log(response);
          refresh();
        })
    };

    $scope.edit = function(id){
      console.log(id);
      $scope.up = true;
      $http.get('/contactlist/' + id)
        .success(function(response){
          $scope.contact = response;
        })
    };

    $scope.update = function(){
      console.log($scope.contact._id);
      $scope.up = false;
      $http.put('/contactlist/' + $scope.contact._id, $scope.contact)
        .success(function(response){
          refresh();
        })
    };




    // person1 = {
    //   name: 'Mauro',
    //   email: 'x@x.cl',
    //   number: '83213540'
    // };
    // person2 = {
    //   name: 'Karen',
    //   email: 'karen@verdugo.cl',
    //   number: '123123'
    // };
    //
    // var contactList = [person1, person2];
    // $scope.contactlist = contactlist;

  });
