$(document).ready(function(){

	// DROPDOWN-MENU
	$('.dropdown').on('show.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).slideDown(200);
	});
	$('.dropdown').on('hide.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp(200);
	});	

	
	// SCROLLSPY
	$('.scrollspy').on('click', function(e) {
	    e.preventDefault();
	    var goto = $(this).attr('target');
	    $('html, body').animate({
	        scrollTop: $(goto).offset().top
	    }, 800);
	}); 

	// CAROUSELL 
	$('.carousel').carousel({
	  interval: 5000
	})

});	//	END READY  


// SCROLL NAVBAR
$(document).scroll(function(e){
    var scrollTop = $(document).scrollTop();
    if(scrollTop > 300){
        $('.cover').removeClass('animation').addClass('animation');
    } else {
        $('.cover').removeClass('animation');
    };
    if(scrollTop > 1005){
        console.log(scrollTop);
        $('.navbar').removeClass('navbar-static-top').slideDown().addClass('navbar-fixed-top, navbar-fixed-top');
        $('#project').addClass('mar-top');
    } else {
    	$('#project').removeClass('mar-top');
        $('.navbar').removeClass('navbar-fixed-top, navbar-fixed-top').addClass('navbar-static-top');
    };

}); 
