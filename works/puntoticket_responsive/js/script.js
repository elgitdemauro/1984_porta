$(document).on('ready', function() {
	//console.log('Connected');


	// off-canvas
	var controller = new slidebars();
	controller.init();
	$( '.toggle-right' ).on( 'click', function ( event ) {
		// Stop default behaviour and propagation
		event.preventDefault();
		event.stopPropagation();
		controller.toggle( 'sb-1' );
	});	
	$( '.close-any' ).on( 'click', function ( event ) {
		// Stop default behaviour and propagation
		event.preventDefault();
		event.stopPropagation();

		controller.close();
	});




	// Slide
	$('.carousel').carousel({
		interval: 400000
	});
	$('.carousel').on('slide.bs.carousel', function (e) {
		var carouselData = $(this).data('bs.carousel');
		var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
		var slideToIndex = $(e.relatedTarget).index();

		// console.log(currentIndex + ' => ' + slideToIndex);

		$('a', $('.listado li')[currentIndex]).removeClass('activo');
		$('a', $('.listado li')[slideToIndex]).addClass('activo');
	});

	$(".slide_img img").each(function() {
		var label = $(this).attr('title');
		$('.listado').append("<li><a href='#' data-target='#carousel-example-generic' data-slide-to=''>"+label+"</a></li>");
	});
	$('.listado li:first a').addClass('activo');  
	$('.listado a').attr('data-slide-to', function(i){
		return i
	});




	// onclick article go a href
	$('.articles.caluga').bind('click', function(){
		location = $(this).find('a').attr('href');
	});




	//radio button - checkbox
	$('input:radio').screwDefaultButtons({
		image: 'url("images/radio_orange.png")',
		width: 15,
		height: 15
	});
	$('input:checkbox').screwDefaultButtons({
		image: 'url("images/checkbox_orange.png")',
		width: 15,
		height: 15
	});




	// checkbox fecha
	$('.btn_fecha .btn-inactivo').on('click', function(){
		$('.btn_fecha .btn-inactivo').not(this).removeClass('btn-warning');
		$(this).addClass('btn-warning');
	});	




	// shipping accordion
	// addClass checked styledRadio Firefox
	$('.shipping label').on('click', function() {
		if ( $(this).children('.rad').attr('aria-checked') == 'true' )  {
			$('label .rad').removeClass('r');
			$(this).children('.rad').addClass('r');
		};
	});
	
	$('.shipping label').on('change', function() {
		if ( $('.shipping .collapse').hasClass('in') ) {		
			$('.shipping .collapse').removeClass('in');
		};
	});
	$('.shipping label').on('change', function() {
		$('label, .panel-body').removeClass('inactivo');
		$('label').not(this).addClass('inactivo')
		if ( $('.rad').attr('aria-checked') == 'true' )  {
			$('label').not(this).addClass('inactivo')
		};
	});




	// select_pay radio
	$('label .grey, .legal').hide();
	$('label img').on('click', function() {
		$('.legal').hide();
		$('#bank'+$(this).attr('target')).fadeIn(400);
	})	
	$('label img.color').on('click', function () {
		$('label img').hide();
		$('label img.grey').show();
		$(this).siblings().hide();
		$(this).show().css('borderColor', '#F47920');
	});
	$('label img.grey').on('click', function () {
		$('label img').hide();
		$('label img.grey').show();
		$(this).siblings('img').show().css('borderColor', '#F47920');
		$(this).hide();
	});




    // checkbox on click label checked .rad
    $('.checkbox label').on('change', function(){
    	if ( $('.rad').attr('aria-checked') == 'true' )  {
    		$(this).children('.rad').attr('aria-checked', 'false');
    	} else{
    		$(this).children('.rad').attr('aria-checked', 'true');
    	}
    });




	// Search jquery.autocomplete 
	$( '#search' ).each( function(){
		$(this).attr( 'title', $(this).val() )
		.focus( function(){
			if ( $(this).val() == $(this).attr('title') ) {
				$(this).val( '' );
			}
		} ).blur( function(){
			if ( $(this).val() == '' || $(this).val() == ' ' ) {
				$(this).val( $(this).attr('title') );
			}
		} );
	} );

	$('input#search').result(function(event, data, formatted) {
		$('#result').html( !data ? "No match!" : "Selected: " + formatted);
	}).blur(function(){		
	});
	
	$(function() {		
		function format(mail) {
			return "<a href='"+mail.permalink+"' target='_self'><img class='img-circle' src='" + mail.image + "' /><span class='title'>" + mail.title +"<br/><small class='date'>"+mail.date+"</small><small class='location'>"+mail.location+"</small></span></a>";			
		}

		function link(mail) {
			return mail.permalink
		}

		function title(mail) {
			return mail.title
		}

		$.Autocompleter.defaults = {
			inputClass: "ac_input",
			resultsClass: "ac_results",
			loadingClass: "ac_loading",
			minChars: 1,
			delay: 400,
			matchCase: false,
			matchSubset: true,
			matchContains: false,
			cacheLength: 100,
			max: 1000,
			mustMatch: false,
			extraParams: {},
			selectFirst: true,
			formatItem: function(row) { return row[0]; },
			formatMatch: null,
			autoFill: false,
			width: 0,
			multiple: false,
			multipleSeparator: " ",
			inputFocus: true,
			clickFire: false,
			highlight: function(value, term) {
				return value.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
			},
			scroll: true,
			scrollHeight: 180,
			scrollJumpPosition: true
		};

		$("#search").autocomplete(completeResults, {
			width: $("#search").outerWidth()-2,			
			max: 5,			
			scroll: false,
			dataType: "json",
			matchContains: "word",
			minChars:2,
			parse: function(data) {
				return $.map(data, function(row) {
					return {
						data: row,
						value: row.title,
						result: $("#search").val()
					}
				});
			},
			formatItem: function(item) {				
				return format(item);
			}
		}).result(function(e, item) {
			$("#search").val(title(item));
			//location.href = link(item);
		});						
	});




	// VER MÁS
	$('.show-more').readmore({
		collapsedHeight: 210,
		speed: 300,
		moreLink: '<a href="#" class="show-more  btn btn-default default pull-right" >Ver m&aacute;s</a>',
		lessLink: '<a href="#" class="show-more btn btn-default default pull-right scrollspy" >Ver menos</a>'
	});	




	// Puntos de Venta
	$('.map2, .c_1').hide();
	$('.c_2.map2').show();

	$('.map_regiones li').on('click', function() {
		event.preventDefault();
		$('.map2').hide();
		$('.map1').show();
		$(this).find('.map1').hide();
		$(this).find('.map2').show();
	});




	// resize mi_cuenta .selector
	$(window).resize(function() {
		var viewportWidth = $(window).width();
	    //console.log(viewportWidth);
	    if ( viewportWidth < 753 ){
	        $('.selector').removeClass('nav');
	        $('.selector').removeClass('nav-tabs');
	        $('.selector').addClass('dropdown-menu')
			
			$('.selector').on('click', function(){
				$(this).children().removeClass('active');
			})
	    } else {
	    	$('.selector').removeClass('dropdown-menu');
	    	$('.selector').addClass('nav');
	        $('.selector').addClass('nav-tabs');	        
	    }
	});
	$(window).trigger('resize');




	// Mi Cuenta, direcciones on click
	$('.article_address').on('click', function(){
		$('.article_address').removeClass('address_active')
		$(this).addClass('address_active')
	})




}); 
// END ON READY




// button +/-
$('.btn-number').click(function(e){
	e.preventDefault();
	
	fieldName = $(this).attr('data-field');
	type      = $(this).attr('data-type');
	var input = $("input[name='"+fieldName+"']");
	var currentVal = parseInt(input.val());
	if (!isNaN(currentVal)) {
		if(type == 'minus') {
			
			if(currentVal > input.attr('min')) {
				input.val(currentVal - 1).change();
			} 
			if(parseInt(input.val()) == input.attr('min')) {
				$(this).attr('disabled', true);
			}

		} else if(type == 'plus') {

			if(currentVal < input.attr('max')) {
				input.val(currentVal + 1).change();
			}
			if(parseInt(input.val()) == input.attr('max')) {
				$(this).attr('disabled', true);
			}

		}
	} else {
		input.val(0);
	}
});
$('.input-number').focusin(function(){
	$(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
	
	minValue =  parseInt($(this).attr('min'));
	maxValue =  parseInt($(this).attr('max'));
	valueCurrent = parseInt($(this).val());
	
	name = $(this).attr('name');
	if(valueCurrent >= minValue) {
		$(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
	} else {
		alert('Sorry, the minimum value was reached');
		$(this).val($(this).data('oldValue'));
	}
	if(valueCurrent <= maxValue) {
		$(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
	} else {
		alert('Sorry, the maximum value was reached');
		$(this).val($(this).data('oldValue'));
	}
});
$(".input-number").keydown(function (e) {
	// Allow: backspace, delete, tab, escape, enter and .
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
		 // Allow: Ctrl+A
		 (e.keyCode == 65 && e.ctrlKey === true) || 
		 // Allow: home, end, left, right
		 (e.keyCode >= 35 && e.keyCode <= 39)) {
			 // let it happen, don't do anything
			return;
		}
	// Ensure that it is a number and stop the keypress
	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
});





//add checked input radio tr map
$('.tabla_precios td').on('click', function(){
	$('tbody tr').removeClass('tr_bg');
	$('tbody tr').find('.input-group').css('display', 'none');

	$(this).parent('tr').addClass('tr_bg');
	$(this).parent('tr').find('.input-group').css('display', 'inline-table');
	$(this).parent('tr').find('#input').prop('checked', true);
});




// scrollspy
$('.scrollspy').on('click', function(e) {
	e.preventDefault();

	var goto = $(this).attr('target');

	$('html, body').animate({
		scrollTop: $(goto).offset().top
	}, 800);
});







